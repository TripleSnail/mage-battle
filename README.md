# Mage Battle #
Mage Battle is 3D realtime multiplayer MOBA game inspired by Warlock Brawl,
which is nice Warcraft III scenario. Game's idea is to use spells to defeat
other players. All spells are skill-shotted.

**NOTE**: Mage Battle is dead and has reborn in the form of Arkhados.
Arkhados already has more features and it's written in Java, using
jMonkeyEngine 3 (nightly).

Arkhados is hosted at [GitHub](https://github.com/dnyarri/Arkhados)

## Dependencies: ##
Mage Battle uses Python 2.7.x and Panda3D 1.9.x (development version) though it
seems to work perfectly well with 1.8.x .

## Running game: ##
python main.py in src directory

### Running server with GUI: ###
python server.py in src-directory. After that, press "Host game" -button.

### Connecting to server: ###
First make sure that both the game and server is running. Then in game
menu, press "Join game" -button. Then write your nickname and press
enter. After that, write address like "localhost" (without quotes) if
you are hosting on the same computer. After that, press enter. Then
press "Connect"-button. If you did everything corretly, you should be
able to see your nickname in server-program's lobby.


## Contributing ##
Contributions are accepted. Just send me your patch or diff via Issue Tracker
and I'll merge it if I think it's good.

## License ##
Mage Battle's code is GNU GPLv3 (see COPYING) licensed and art assets use
various Creative Commons licenses. More information about art assets' can be
found in doc/multimedia.org .
