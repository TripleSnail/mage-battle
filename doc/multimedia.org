This file contains all information about multimedia that this game
could potentially use or uses:

* 3D-models:

** Floor:
   - Author: William Linna
   - License: CC0

** Firecube:
   - Author: William Linna
   - License: CC0

** Animated Mage:
   - Author: Clement Wu, Nikolaus & Botanic
   - Licenses:
     - CC-BY 3.0
     - CC-BY-SA 3.0
     - GPL 2.0
     - GPL 3.0
   - Link: http://opengameart.org/content/animated-mage


* Music:
  nothing yet

* Sound effects:
  nothing yet
