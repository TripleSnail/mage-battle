#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

#-*- coding: utf-8 -*-

import os
import json
import glob
from panda3d.core import Vec3, TextNode, Vec4
from direct.gui.DirectGui import DirectLabel, DirectButton, DirectEntry
from direct.fsm.FSM import FSM


class Menu(object):
    """
    """

    def __init__(self, menuDict, commands):
        """
        """

        self.menuDict = menuDict
        self.name = self.menuDict["name"]
        self.controls = {}
        self.onscreenTexts = []
        self.active = False
        self.commands = commands

    def activate(self):
        for lbl in self.menuDict['labels']:
            createLabel(lbl, self)

        for btn in self.menuDict['buttons']:
            createButton(btn, self)

        for ent in self.menuDict['entries']:
            createEntry(ent, self)

    def deactivate(self):
        for k, c in self.controls.items():
            c.destroy()
            self.controls.pop(k)

        while self.onscreenTexts:
            ost = self.onscreenTexts.pop()
            ost.destroy()


def initMenus(commands, folderName):
    """
    """
    menus = {}
    menuFilenames = glob.glob("./menus/" + folderName + "/*")
    for mFilename in menuFilenames:
        f = open(mFilename, 'r')
        menuDict = json.loads(f.read())
        f.close()
        menu = Menu(menuDict, commands)
        menus[menu.name] = menu

    return menus


def createLabel(jsonDict, menu):
    name = jsonDict[u'name']
    text = jsonDict[u'text']

    pos = Vec3(jsonDict[u'pos'][u'x'], jsonDict[u'pos'][u'y'], jsonDict[u'pos'][u'z'])
    scale = jsonDict[u'scale']

    textFg = jsonDict[u'text_fg']
    textFgTuple = (textFg[u'r'], textFg[u'g'], textFg[u'b'], textFg[u'a'])
    alignStr = jsonDict[u'align'] if u'align' in jsonDict else ""
    align = TextNode.ACenter
    if alignStr == "left":
        align = TextNode.ALeft
    elif alignStr == "right":
        align = TextNode.ARight
    frameColor = jsonDict[u'frameColor']
    frameColorTuple = jsonDict[u'frameColor']
    frameColor = Vec4(frameColorTuple[u'r'], frameColorTuple[u'g'],
                      frameColorTuple[u'b'], frameColorTuple[u'a'])

    control = DirectLabel(text=text,
                          pos=pos,
                          scale=scale,
                          text_align=align,
                          text_fg=textFgTuple,
                          frameColor=frameColor)
    menu.controls[name] = control


def createButton(jsonDict, menu):
    name = jsonDict[u'name']
    command = menu.commands[jsonDict[u'command']][0]
    extraArgs = menu.commands[jsonDict[u'command']][1]
    text = jsonDict[u'text']
    pos = Vec3(jsonDict[u'pos'][u'x'], jsonDict[u'pos'][u'y'], jsonDict[u'pos'][u'z'])
    scale = jsonDict[u'scale']

    frameColorTuple = jsonDict[u'frameColor']
    frameColor = Vec4(frameColorTuple[u'r'], frameColorTuple[u'g'],
                      frameColorTuple[u'b'], frameColorTuple[u'a'])
    control = DirectButton(text=text,
                           command=command,
                           extraArgs=extraArgs,
                           pos=pos,
                           scale=scale,
                           frameColor=frameColor)
    menu.controls[name] = control


def createEntry(jsonDict, menu):
    name = jsonDict[u'name']
    initialText = jsonDict[u'initial_text'] if u'initial_text' in jsonDict else ""
    pos = Vec3(jsonDict[u'pos'][u'x'], jsonDict[u'pos'][u'y'], jsonDict[u'pos'][u'z'])
    scale = jsonDict[u'scale']
    width = int(jsonDict[u'width']) if u'width' in jsonDict else 10

    # alignStr = jsonDict[u'align'] if u'align' in jsonDict else ""
    # align = TextNode.ACenter
    # if alignStr == "left":
    #     align = TextNode.ALeft
    # elif alignStr == "right":
    #     align = TextNode.ARight

    cmdStr = jsonDict[u'command']
    command = menu.commands[cmdStr][0]
    focus = jsonDict[u'focus'] if u'focus' in jsonDict else 0
    # focusOutCommand = menu.commands[jsonDict[u'focusOutCommand']][0]
    # frameColorTuple = jsonDict[u'frameColor']
    # frameColor = Vec4(frameColorTuple[u'r'], frameColorTuple[u'g'],
    #                   frameColorTuple[u'b'], frameColorTuple[u'a'])
    control = DirectEntry(initialText=initialText,
                          pos=pos,
                          scale=scale,
                          # focusOutCommand=focusOutCommand,
                          width=width,
                          # text_align=align,
                          command=command,
                          focus=focus)
    menu.controls[name] = control
