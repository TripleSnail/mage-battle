#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

#-*- coding: utf-8 -*-
import itertools
import logging

from direct.showbase.DirectObject import DirectObject


class GameObject(object, DirectObject):
    """ Base-class for all classes that need to be synced somehow
    """
    counter = itertools.count(0)

    def __init__(self, position, objType, owner=None):
        """
        """
        self.posCurrent = position
        self.posTarget = position
        self.objType = objType
        self.owner = owner
        self.id = GameObject.counter.next()

    def __del__(self):
        logging.info('Garbage collected GameObject with id ' + str(self.id))

    def load(self, dic):
        """

        Arguments:
        - `dic`:
        """
        self.objType = dic[u'object_type']

    def update(self):
        """ Updates object
        """
        logging.error("ERROR: update not overridden for game object with id " + str(self.id))
        raise

    def sync(self, data):
        """ Syncs object's state
        """
        logging.error("ERROR: sync not overridden for game object with id " + str(self.id))
        raise

    def destroy(self):
        """ Ensures that all resources that object uses can be freed
        """
        self.ignoreAll()
        self.removeAllTasks()
