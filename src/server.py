#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

#-*- coding: utf-8 -*-
import os
import logging

from panda3d.core import loadPrcFileData
from panda3d.core import TextNode
from panda3d.core import PointerToConnection, QueuedConnectionListener

from direct.showbase.DirectObject import DirectObject
from direct.gui.OnscreenText import OnscreenText
from direct.fsm.FSM import FSM

import net
import battle
import protocols
import menu
from player import Player
import helper
import packet
import global_vars


class Server(net.NetCommon):
    """
    """

    def __init__(self, protocol):
        """
        """
        net.NetCommon.__init__(self, protocol)
        self.listener = QueuedConnectionListener(self.manager, 0)
        self.connections = []
        self.players = []
        self.protocol.setServer(self)
        self.active = False
        self.battle = None
        self.packetHelper = packet.ServerPacketHelper(self)

    def start(self, port):
        socket = self.manager.openTCPServerRendezvous(port, 100)
        self.listener.addConnection(socket)
        taskMgr.add(self.updateListener, "update_listener")

    def stop(self):
        del self.connections[:]
        del self.players[:]
        self.listener = None
        taskMgr.remove("update_listener")

    def updateListener(self, task):
        """
        """
        if self.listener.newConnectionAvailable():
            connection = PointerToConnection()
            if self.listener.getNewConnection(connection):
                connection = connection.p()
                self.connections.append(connection)
                self.reader.addConnection(connection)
                logging.info("New connection established")
                self.players.append(Player(connection))

        for conn in self.connections[:]:
            if not self.reader.isConnectionOk(conn):
                logging.warning("Non-ok connection detected")
                for player in self.players:
                    if player.connection == conn:
                        self.players.remove(player)
                        nick = player.nick
                        logging.info("Removed player with non-ok connection")
                        messenger.send("player_disconnected", sentArgs=[nick])
                self.connections.remove(conn)
        return task.cont

    def startGame(self):
        self.battle = battle.Battle({p.nick: p for p in self.players})
        messenger.send("send_start_game", sentArgs=[self.players])
        self.battle.setupServerWorld()


    def broadcast(self, datagram):
        for conn in self.connections:
            self.writer.send(datagram, conn)

    def sendToSelectedPlayers(self, datagram, players):
        for p in players:
            conn = p.connection
            self.writer.send(datagram, conn)

    def destroy(self):
        """
        """
        net.NetCommon.destroy(self)
        taskMgr.remove("update_listener")


class ServerGuiState(object, FSM):
    """
    """

    def __init__(self, gui):
        """
        """
        self.gui = gui
        FSM.__init__(self, "gui_state")

    def enterMainMenu(self):
        self.gui.currentMenu = self.gui.menus['main']
        self.gui.currentMenu.activate()

        if self.gui.server.active:
            self.gui.server.stopServer()

    def exitMainMenu(self):
        self.gui.menus['main'].deactivate()
        self.gui.currentMenu = None

    def enterLobby(self):
        self.gui.currentMenu = self.gui.menus['lobby']
        self.gui.currentMenu.activate()
        self.gui.hostGame()

    def exitLobby(self):
        self.gui.menus['lobby'].deactivate()

    def enterGame(self):
        self.gui.currentMenu.deactivate()

    def exitGame(self):
        self.gui.currentMenu.activate()


class ServerUI(object, DirectObject):
    """
    """
    def __init__(self, state):
        self.server = Server(protocols.ServerProtocol())
        self.state = state
        self.serverActive = False
        self.accept("player_connected", self.playerConnected)
        self.accept("player_disconnected", self.playerDisconnected)
        self.accept("received_chat_message", self.receivedChatMessage)

    def hostGame(self):
        self.server.start(12345)
        self.serverActive = True

    def destroy(self):
        self.server.destroy()
        self.server = None
        self.ignoreAll()


class ServerGUI(ServerUI):
    """
    """

    def __init__(self):
        """
        """
        base.setBackgroundColor(0, 0, 0)
        base.disableMouse()
        self.currentMenu = None
        ServerUI.__init__(self, ServerGuiState(self))

        self.commands = {"startServer": (self.state.request, ["Lobby"]),
                         "exitLobby": (self.state.request, ["MainMenu"]),
                         "enterMessage": (self.enterMessage, []),
                         "entNothing": (self.entNothing, []),
                         "startGame": (self.startGame, []),
                         "nothing": (self.nothing, [])}
        self.menus = menu.initMenus(self.commands, "server")
        self.state.request("MainMenu")

    def playerConnected(self, nick):
        if self.state.state == "Lobby":
            self.refreshLobby()

    def playerDisconnected(self, nick):
        if self.state.state == "Lobby":
            self.refreshLobby()

    def receivedChatMessage(self, nick, message):
        if self.state.state == "Lobby":
            self.refreshLobby()

    def refreshLobby(self):
        for ost in self.currentMenu.onscreenTexts:
            ost.destroy()

        self.currentMenu.onscreenTexts = []

        for i in xrange(len(self.server.players)):
            player = self.server.players[i]
            height = 0.65 - i * 0.1
            ost = OnscreenText(text=player.nick,
                               pos=(-1.2, height),
                               scale=0.07,
                               align=TextNode.ALeft,
                               fg=(0, 1, 0, 1))
            self.currentMenu.onscreenTexts.append(ost)

        showMessagesLen = helper.clamp(len(self.server.chatLog), 0, 10)
        for i in xrange(showMessagesLen):
            nick = self.server.chatLog[i][0]
            msg = self.server.chatLog[i][1]
            y = 0 + (i - showMessagesLen + 1) * 0.07
            text = "<" + nick + "> " + msg
            ost = OnscreenText(text=text,
                               pos=(-1.2, y),
                               scale=0.05,
                               align=TextNode.ALeft,
                               fg=(1, 1, 1, 1))
            self.currentMenu.onscreenTexts.append(ost)

    def enterMessage(self, message):
        messenger.send("send_chat_message", sentArgs=[message, "all"])
        self.server.addToChatLog("server", message)
        self.currentMenu.controls['ent_chat'].enterText("")
        self.currentMenu.controls['ent_chat']['focus'] = 1
        self.refreshLobby()

    def startGame(self):
        self.server.startGame()
        self.state.request("Game")

    def endGame(self):
        self.state.request("Lobby")

    def entNothing(self, text):
        pass

    def nothing(self):
        pass

if __name__ == '__main__':
    loadPrcFileData('', 'window-title Server')
    loadPrcFileData('', 'win-origin 1100 250')
    global_vars.DUMB = False
    import direct.directbase.DirectStart
    logging.basicConfig(filename=os.path.join("..", "server.log"),
                        level=logging.DEBUG)
    logging.info("--------------------- PROGRAM STARTED ------------------")
    serverGUI = ServerGUI()
    run()
