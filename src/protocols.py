#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

#-*- coding: utf-8 -*-
import collections
import logging

from panda3d.core import Point3
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator

import packet
import player


class ServerMsgId(object):
    keepAlive = 0
    registration = 1
    chatMsg = 2
    mouseClick = 3
    spellCastTry = 4


class ClientMsgId(object):
    keepAlive = 0
    joinAccepted = 1
    playerConnected = 2
    playerDisconnected = 3
    chatMsg = 4
    gameStarted = 5
    myWarlockId = 6
    sync = 7
    createObject = 8
    removeObject = 9


class Protocol(object):
    """
    """

    def process(self, data):
        return None

    def logMessage(self, title, msg):
        logging.info("Logged message - %s: %s" % (title, msg))

    def buildListStr(self, dataList):
        dataStr = dataList[0]
        for data in dataList[1:]:
            dataStr += "$" + str(data)

        return dataStr

    def buildReply(self, msgid, data):
        reply = PyDatagram()
        reply.addUint8(msgid)
        reply.addString(data)
        return reply


class ServerProtocol(Protocol):
    def __init__(self):
        self.server = None

    def setServer(self, server):
        self.server = server

    def process(self, data):
        it = PyDatagramIterator(data)
        msgid = it.getUint8()
        conn = data.getConnection()

        sendingPlayer = None
        for p in self.server.players:
            if p.connection == conn:
                sendingPlayer = p
                break

        else:
            logging.error("No sendingPlayer found")

        if msgid == ServerMsgId.keepAlive:
            return None
        elif msgid == ServerMsgId.registration:
            return self.handleRegistration(it, sendingPlayer)

        elif msgid == ServerMsgId.chatMsg:
            return self.handleChatMsg(it, sendingPlayer)

        elif msgid == ServerMsgId.mouseClick:
            return self.handleMouseClick(it, sendingPlayer)

        elif msgid == ServerMsgId.spellCastTry:
            return self.handleSpellCastTry(it, sendingPlayer)

        # elif msgid == :
        #     return self.handleBye(it)

    def handleRegistration(self, it, player):
        nick = it.getString()
        player.nick = nick
        messenger.send("player_connected", sentArgs=[nick])
        connectedNicks = [p.nick for p in self.server.players]
        nicksStr = self.buildListStr(connectedNicks)
        return self.buildReply(ClientMsgId.joinAccepted, nicksStr)

    def handleChatMsg(self, it, player):
        nick = player.nick
        message = it.getString()
        self.server.addToChatLog(nick, message)
        messenger.send("received_chat_message", sentArgs=[nick, message])
        datagram = PyDatagram()
        datagram.addUint8(ClientMsgId.chatMsg)
        datagram.addString(nick)
        datagram.addString(message)
        self.server.broadcast(datagram)

    def handleMouseClick(self, it, player):
        button = it.getString()
        position = Point3()
        position.readDatagramFixed(it)
        self.server.battle.world.handleMouseInput(player, button, position)

    def handleSpellCastTry(self, it, player):
        spellName = it.getString()
        self.server.battle.world.handleSpellCastTry(player, spellName)


class ClientProtocol(Protocol):
    def __init__(self):
        self.client = None

    def setClient(self, client):
        self.client = client

    def process(self, data):
        it = PyDatagramIterator(data)
        msgid = it.getUint8()

        if msgid == ClientMsgId.keepAlive:
            return None

        elif msgid == ClientMsgId.joinAccepted:
            return self.handleJoinAccepted(it)

        elif msgid == ClientMsgId.playerConnected:
            return self.handleConnect(it)

        elif msgid == ClientMsgId.playerDisconnected:
            return self.handleDisconnect(it)

        elif msgid == ClientMsgId.chatMsg:
            return self.handleChatMsg(it)

        elif msgid == ClientMsgId.gameStarted:
            return self.handleGameStarted(it)

        elif msgid == ClientMsgId.myWarlockId:
            return self.handleMyWarlockId(it)

        elif msgid == ClientMsgId.sync:
            return self.handleSync(it)

        elif msgid == ClientMsgId.createObject:
            return self.handleCreateObject(it)

        elif msgid == ClientMsgId.removeObject:
            return self.handleRemoveObject(it)

    def handleJoinAccepted(self, it):
        nicks = it.getString().split('$')
        self.client.nicks = nicks
        messenger.send("join_accepted")

    def handleConnect(self, it):
        nick = it.getString()
        self.client.nicks.append(nick)
        messenger.send("player_connected", [nick])

    def handleDisconnect(self, it):
        nick = it.getString()
        self.client.nicks.remove(nick)
        messenger.send("player_disconnected", [nick])

    def handleChatMsg(self, it):
        nick = it.getString()
        msg = it.getString()
        text = '<' + nick + '> ' + msg
        self.client.addToChatLog(nick, msg)
        messenger.send("chat_msg_received")

    def handleGameStarted(self, it):
        self.client.nicks = it.getString().split('$')
        logging.info("Joined players: " + repr(self.client.nicks))

        for n in self.client.nicks:
            self.client.players[n] = player.Player(nick=n)
        messenger.send("game_started")
        logging.info("Game started")

    def handleMyWarlockId(self, it):
        id = it.getUint16()
        messenger.send("my_warlock_id", [id])

    def handleSync(self, it):
        amountOfCharacters = it.getUint16()
        for i in xrange(amountOfCharacters):
            id = it.getUint16()
            hpCurrent = it.getUint16()
            position = Point3()
            position.readDatagramFixed(it)
            messenger.send("sync_id", [[id, hpCurrent, position]])

    def handleCreateObject(self, it):
        objName = it.getString()
        position = Point3()
        position.readDatagramFixed(it)
        # Nick of owner
        nick = ""
        containsOwner = it.getBool()
        if containsOwner:
            nick = it.getString()
        messenger.send("create_object", [objName, position, nick])

    def handleRemoveObject(self, it):
        id = it.getUint16()
        messenger.send("remove_object", sentArgs=[id])
