#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.
import logging
from direct.distributed.PyDatagram import PyDatagram

from protocols import ServerMsgId

import net
import packet


class Client(net.NetCommon):
    """
    """

    def __init__(self, protocol):
        """

        Arguments:
        - `protocol`:
        """
        net.NetCommon.__init__(self, protocol)
        self.connection = None
        self.players = {}
        self.nicks = []
        self.player = None
        self.protocol.setClient(self)
        self.packetHelper = packet.ClientPacketHelper(self)

    def connect(self, host, port, timeout, nick):
        """

        Arguments:
        - `host`:
        - `port`:
        - `timeout`:
        """
        self.connection = self.manager.openTCPClientConnection(host, port, timeout)
        if self.connection:
            self.reader.addConnection(self.connection)
            registrationDatagram = PyDatagram()
            registrationDatagram.addUint8(ServerMsgId.registration)
            registrationDatagram.addString(nick)
            self.send(registrationDatagram)
            logging.info("Connected to server")
            return True
        return False

    def disconnect(self):
        if self.connection:
            self.manager.closeConnection(self.connection)

    def send(self, datagram):
        """

        Arguments:
        - `datagram`:
        """
        if self.connection:
            self.writer.send(datagram, self.connection)
