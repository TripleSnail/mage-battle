#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

#-*- coding: utf-8 -*-
from panda3d.core import *
from direct.showbase.ShowBase import ShowBase
from direct.showbase.DirectObject import DirectObject
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator

import protocols
from protocols import ServerMsgId, ClientMsgId

class NetCommon(object, DirectObject):
    """
    """

    def __init__(self, protocol):
        """

        Arguments:
        - `protocol`:
        """
        self.protocol = protocol
        self.manager = ConnectionManager()
        self.reader = QueuedConnectionReader(self.manager, 0)
        self.writer = ConnectionWriter(self.manager, 0)
        self.chatLog = []
        taskMgr.add(self.updateReader, "update_reader")

    def updateReader(self, task):
        if self.reader.dataAvailable():
            data = NetDatagram()
            self.reader.getData(data)
            reply = self.protocol.process(data)

            if reply is not None:
                self.writer.send(reply, data.getConnection())

        return task.cont

    def addToChatLog(self, nick, msg):
        self.chatLog.insert(0, (nick, msg))

    def destroy(self):
        taskMgr.remove("update_reader")
        self.ignoreAll()
