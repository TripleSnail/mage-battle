#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
from direct.showbase.DirectObject import DirectObject
from direct.task import Task
from panda3d.core import (Point3, Vec3, Plane,
                          CollisionPlane, CollisionTraverser, CollisionNode,
                          CollisionRay, CollisionHandlerQueue, GeomNode)

import manager

OBJ_SYNC_INTERVAL = 1.0 / 50.0


class World(object, DirectObject):
    """ World is responsible on showing and updating world and all
    it's objects. World also handles user-input.
    """
    def __init__(self, players={}, thisPlayer=None):

        # gameObjectManager sets this
        self.caster = None
        self.gameObjectManager = manager.ClientGameObjectManager(self, thisPlayer)
        self.setupFloor()
        taskMgr.add(self.controlCameraTask, "ControlCameraTask")
        taskMgr.add(self.update, "update")

        self.characters = self.gameObjectManager.characters
        self.gameObjectManager.players = players
        self.thisPlayer = thisPlayer
        self.networkingOn = False

        self.networkingOn = True

        self.setupCollisionDetection()
        self.accept("mouse1", self.mouse1Event)
        # self.accept("escape", self.exit)

    def exit(self):
        pass

    def setupCollisionDetection(self):
        """
        """
        base.cTrav = CollisionTraverser()
        #base.cTrav.showCollisions(render)

        self.mouseRayHandler = CollisionHandlerQueue()

        self.pickerNode = CollisionNode("pickerRay")
        self.pickerNP = camera.attachNewNode(self.pickerNode)
        self.pickerNode.setFromCollideMask(GeomNode.getDefaultCollideMask())
        self.pickerNode.setIntoCollideMask(0)
        self.pickerRay = CollisionRay()
        self.pickerNode.addSolid(self.pickerRay)
        base.cTrav.addCollider(self.pickerNP, self.mouseRayHandler)

    def setupFloor(self):
        """
        """
        self.floor = loader.loadModel("../data/common/assets/terrain.egg")
        self.floor.reparentTo(render)
        self.floor.setScale(100, 100, 1)
        self.floor.setPos(0, 0, 0)
        self.floor.setTag("object_type", "floor")

        collider = self.floor.attachNewNode(CollisionNode("floor"))
        collider.node().addSolid(CollisionPlane(Plane(Vec3(0, 0, 1),
                                                      Point3(0, 0, 0.1))))

    def setupCasters(self, players):
        for k in players.keys():
            char = manager.DataManager.loadObject("warlock", owner=players[k])
            self.characters.append(char)
            self.gameObjectManager.addGameObject(char)

    def setPlayersCaster(self, id=-1):
        for c in self.characters:
            if c.owner.connection is not None:
                self.caster = c

    def controlCameraTask(self, task):
        camera.setPos(0, -90, 140)
        camera.setHpr(0, -60, 0)
        return task.done

    def update(self, task):
        for c in self.characters:
            c.update()

        return task.cont

    def mouse1Event(self):
        if base.mouseWatcherNode.hasMouse():
            mpos = base.mouseWatcherNode.getMouse()
            self.pickerRay.setFromLens(base.camNode, mpos.getX(), mpos.getY())
            base.cTrav.traverse(render)
            entriesAmount = self.mouseRayHandler.getNumEntries()

            if entriesAmount > 0:
                self.mouseRayHandler.sortEntries()
                entry = self.mouseRayHandler.getEntry(0)
                self.clickToObject(entry)

    def clickToObject(self, entry):
        """
        """
        objectNP = entry.getIntoNodePath()
        pickedObject = objectNP.findNetTag("object_type")
        if not pickedObject.isEmpty():
            objectType = pickedObject.getTag("object_type")
            if objectType == "floor":
                location = entry.getSurfacePoint(render)
                if self.networkingOn:
                    messenger.send('clicked_on_floor', ["m1", location])

                walkTo = self.caster.walkTo
                self.caster.doAction(walkTo, location)
            elif objectType in ["character", "caster", "warlock"]:
                pickedObject.getPythonTag("owner").logInfo()

    def destroy(self):
        taskMgr.remove("update")
        self.ignoreAll()
        for c in self.characters:
            c.destroy()
        self.floor.removeNode()


class ServerWorld(object, DirectObject):
    """ Same as World but has some extra server-side functionality and
    doesn't have useless client-side code
    """

    def __init__(self):
        """
        """
        self.characters = []
        self.gameObjects = {}
        self.gameObjectManager = manager.ServerGameObjectManager(self)
        self.characters = self.gameObjectManager.characters
        self.gameObjects = self.gameObjectManager.gameObjects
        self.setupCollisionDetection()
        self.setupFloor()

        taskMgr.add(self.controlCameraTask, "control_camera_task")
        taskMgr.add(self.update, "update", sort=1)
        taskMgr.doMethodLater(OBJ_SYNC_INTERVAL, self.syncObjects,
                              "sync_objects", sort=2)

        self.accept('add_game_object', self.addGameObject)

    def setupCasters(self, players):
        self.gameObjectManager.players = players
        for nick, p in players.iteritems():
            warlock = manager.DataManager.loadObject("warlock", owner=p)
            messenger.send("send_create_object",
                           sentArgs=["warlock", warlock.posCurrent, p])
            p.caster = warlock
            self.characters.append(warlock)
            self.gameObjects[warlock.id] = warlock

    def setupCollisionDetection(self):
        base.cTrav = CollisionTraverser()
        base.cTrav.showCollisions(render)

    def setupFloor(self):
        self.floor = loader.loadModel("../data/common/assets/terrain.egg")
        self.floor.reparentTo(render)
        self.floor.setScale(100, 100, 1)
        self.floor.setPos(0, 0, 0)
        self.floor.setTag("object_type", "floor")

        collider = self.floor.attachNewNode(CollisionNode("floor"))
        collider.node().addSolid(CollisionPlane(Plane(Vec3(0, 0, 1),
                                                      Point3(0, 0, 0.1))))

    def addGameObject(self, gameObj):
        self.gameObjects[gameObj.id] = gameObj

    def controlCameraTask(self, task):
        camera.setPos(0, -90, 140)
        camera.setHpr(0, -60, 0)
        return task.done

    def handleMouseInput(self, player, button, position):
        walkTo = player.caster.walkTo
        player.caster.doAction(walkTo, position)

    def handleSpellCastTry(self, player, spellName):
        castSpell = player.caster.castSpell
        player.caster.doAction(castSpell)

    def syncObjects(self, task):
        messenger.send("sync_objects", sentArgs=[self.gameObjects])
        return task.again

    def update(self, task):
        for c in self.characters:
            c.update()
        return task.cont

    def destroy(self):
        self.ignoreAll()
        for c in self.characters:
            c.destroy()
        del self.characters[:]
        self.floor.removeNode()
