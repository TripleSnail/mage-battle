#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

import math
import os
import posixpath
from panda3d.core import Vec2
from direct.actor.Actor import Actor

dataDirectory = posixpath.join("..", "data")


def almostEqual(a, b, tolerance=0.01):
    return abs(a - b) < tolerance


def angleToVec2(angle):
    """
    """
    angleRad = math.radians(angle)
    x = math.sin(angleRad)
    y = -math.cos(angleRad)

    # x = math.cos(angleRad)
    # y = math.sin(angleRad)

    return Vec2(x, y)


def vec2ToAngle(vec):
    """
    """
    x = vec.getX()
    y = vec.getY()
    if x == 0 and y == 0:
        return 0
    angle = math.degrees(math.atan2(y, x)) + 90
    if angle < 0:
        angle += 360
    # if angle > 180:
    #     angle = -180 +  (angle - 180)

    return angle % 360


def clamp(val, minVal, maxVal):
    """
    """
    retVal = val
    if val > maxVal:
        retVal = maxVal
    elif val < minVal:
        retVal = minVal

    return retVal


def setDataPath(p):
    global dataDirectory
    dataDirectory = posixpath.abspath(p)


def getDataPath():
    global dataDirectory
    return dataDirectory


def makeActor(actorDic):
    directory = posixpath.join(
        dataDirectory, posixpath.join(*actorDic[u'directory']))
    modelName = posixpath.join(directory, actorDic[u'model'])

    animationsDic = actorDic[u'animations']

    for k, v in animationsDic.items():
        animationsDic[k] = posixpath.join(directory, v)

    scale = actorDic[u'scale']
    actor = Actor(modelName, animationsDic)
    actor.setScale(**scale)

    return actor


def makeModel(modelDic):
    directory = posixpath.join(
        dataDirectory, posixpath.join(*modelDic[u'directory']))
    modelName = posixpath.join(directory, modelDic[u'model'])
    scale = modelDic[u'scale']

    model = loader.loadModel(modelName)
    model.setScale(**scale)

    return model
