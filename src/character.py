#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

import logging
import copy
from panda3d.core import Vec3, CollisionNode, CollisionSphere

import gameobj
import spell
import helper
import global_vars


class Character(gameobj.GameObject):
    """
    """

    def __init__(self, position, objType="character", owner=None):
        """
        """
        gameobj.GameObject.__init__(self, position, objType, owner)
        self._animCurrent = "idle"
        self.headingCurrent = helper.vec2ToAngle(self.posTarget.getXy() -
                                                 self.posCurrent.getXy())
        self.headingTarget = self.headingCurrent
        self.buffs = []
        self.immobilized = 0
        self.silenced = 0
        self.invisible = 0
        self.buffTimer = 0.0

    def logInfo(self):
        logging.info("INFO of character with id %u\n"
                     "Owner id: %u; owner's nick %s" % (self.id, self.owner.id, self.owner.nick))

    def load(self, dic):
        """

        Arguments:
        - `dic`: dictionary that contains all data needed to load character
        """

        self.hpMax = dic[u'health_max']
        self._hpCurrent = self.hpMax
        self.speedMovement = dic[u'speed_movement']
        self.speedRotation = dic[u'speed_rotation']

        actorDic = dic[u'actor']

        self.actor = helper.makeActor(actorDic)
        self.actor.reparentTo(render)
        self.actor.setPos(self.posCurrent)
        self.actor.loop(self._animCurrent)

        self.actor.setTag("object_type", self.objType)
        self.actor.setPythonTag("owner", self)

        modelBounds = self.actor.getChild(0).getBounds()
        center = modelBounds.getCenter()
        radius = modelBounds.getRadius() * 0.5

        self.collider = self.actor.attachNewNode(CollisionNode('character'))
        # TODO: Use better CollisionSolid, perhaps CollisionTube
        self.collider.node().addSolid(CollisionSphere(center, radius))
        self.collider.setPythonTag("owner", self)
        self.collider.node().setTag("object_type", self.objType)

    def update(self):

        dt = globalClock.getDt()

        self.handleMovement()
        if self.buffTimer >= 0.1:
            self.buffTimer = 0
            self.manageBuffs()
        else:
            self.buffTimer += dt

    def doAction(self, func, *args, **kwargs):
        """

        Arguments:
        - `func`:
        """
        if self.hpCurrent > 0:
            func(*args, **kwargs)

    def handleMovement(self):
        """

        Arguments:
        - `task`:
        """
        dt = globalClock.getDt()
        directionCurrent = helper.angleToVec2(self.headingCurrent)
        directionTarget = helper.angleToVec2(self.headingTarget)

        if not directionCurrent.almostEqual(directionTarget, 0.01):
      #if not helper.almostEqual(self.headingCurrent, self.headingTarget, 0.1):
            rot = directionCurrent.signedAngleDeg(directionTarget)
            #rot = self.headingTarget - self.headingCurrent
            #headingPrev = self.headingCurrent
            if rot > 0:
                self.headingCurrent += self.speedRotation * dt

            else:
                self.headingCurrent -= self.speedRotation * dt

        else:
            self.headingCurrent = self.headingTarget

        displacementVec = self.posTarget - self.posCurrent
        if displacementVec.length() > 0.1:
            displacementVec.normalize()
            self.posCurrent += displacementVec * self.speedMovement * dt

        else:
            self.posCurrent = self.posTarget

        # FIXME
        # elif self.hpCurrent > 0:
        #     self.animCurrent = "idle"

        self.actor.setH(self.headingCurrent)
        self.actor.setPos(self.posCurrent)

    def manageBuffs(self):
        """
        """
        buffs = self.buffs  # [:]
        buffsLen = len(buffs)
        for i in reversed(xrange(buffsLen)):
            buff = buffs[i]
            buff.inflict(0.1)
            if not buff.statModifiers:
                self.buffs.pop(i)

    def calcNextHeading(self, posTargetNew):
        """
        """

        posCurrent = self.actor.getPos()

        headingNext = helper.vec2ToAngle(posTargetNew - posCurrent)

        self.headingTarget = headingNext
        self.posTarget = posTargetNew

    def walkTo(self, location):
        """

        Arguments:
        - `location`:
        """
        self.calcNextHeading(location)
        self.animCurrent = "walk"

    def addBuffs(self, buffs):
        for buff in buffs:
            buff.activate(self)
        self.buffs.extend(buffs)

    def sync(self, data):
        hpCurrent = data[1]
        pos = data[2]

        self.hpCurrent = hpCurrent

        if not self.posCurrent.almostEqual(pos, 0.5):
            self.walkTo(pos)
            #self.posCurrent = pos

        #else:
            #self.animCurrent = "walk"

    def changeAnimation(self, newAnim):
        """

        Arguments:
        - `newAnim`:
        """
        if newAnim != self._animCurrent:
            self._animCurrent = newAnim
            if self._animCurrent == "die":
                self.actor.play(newAnim)
            else:
                self.actor.loop(newAnim)

    def hpCurrent(self, value):
        self._hpCurrent = value
        if self._hpCurrent <= 0:
            self._hpCurrent = 0
            self.animCurrent = 'die'
            logging.info('Object with id ' + str(self.id) + ' should die')

    @property
    def hpCurrent(self):
        return self._hpCurrent

    @hpCurrent.setter
    def hpCurrent(self, value):
        self._hpCurrent = value
        if self._hpCurrent <= 0:
            self._hpCurrent = 0
            self.animCurrent = "die"

    animCurrent = property(lambda self: self._animCurrent, changeAnimation)

    def destroy(self):
        """
        """
        logging.info("Destroying character with id " + str(self.id))
        gameobj.GameObject.destroy(self)
        self.actor.clearPythonTag("owner")
        self.collider.clearPythonTag("owner")
        self.actor.cleanup()
        self.actor.removeNode()
        self.collider.removeNode()

        self.actor.clearPythonData()
        self.actor.delete()


class Warlock(Character):
    """
    """

    def __init__(self, position=None, owner=None):
        """
        """
        Character.__init__(self, position, objType="caster", owner=owner)

        self.spells = [spell.ProjectileSpell.Fireball().select(self)]

    def setPlayerEvents(self):
        self.accept('q', self.castSpell)

    def castSpell(self):
        """
        """
        # TODO: Reorganize spell casting
        messenger.send("tried_to_cast", [self.spells[0].name])
        if not global_vars.DUMB:
            directionVec2 = helper.angleToVec2(self.headingCurrent)
            directionVec3 = Vec3(directionVec2.getX(), directionVec2.getY(), 0)
            posLifted = copy.copy(self.posCurrent) + Vec3(0, 0, 12)
            self.spells[0].cast(posLifted, directionVec3)

    def destroy(self):
        Character.destroy(self)
        spells = self.spells
        while spells:
            spellToDestroy = spells.pop()
            spellToDestroy.destroy()

        self.spells = None
