#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

import os
import logging

from panda3d.core import TextNode, loadPrcFileData
from direct.fsm.FSM import FSM
from direct.gui.OnscreenText import OnscreenText
from direct.showbase.DirectObject import DirectObject
import menu
import client
import protocols
import helper
import battle
import global_vars


class GameState(object, FSM):
    """
    """
    def __init__(self, game):
        """
        """
        self.game = game
        self.currentMenu = None
        FSM.__init__(self, "game_state")

    def enterMatch(self):
        """
        """
        self.game.currentMenu = None
        self.exitMainMenu()

    def exitMatch(self):
        """
        """
        self.game.world.destroy()

    def enterMainMenu(self):
        """
        """
        self.game.currentMenu = self.game.menus["main"]
        self.game.menus["main"].activate()

    def exitMainMenu(self):
        """
        """
        self.currentMenu = None
        self.game.menus["main"].deactivate()

    def enterJoinMenu(self):
        self.game.client.disconnect()
        self.game.currentMenu = self.game.menus["connect"]
        self.game.menus["connect"].activate()

    def exitJoinMenu(self):
        self.currentMenu = None
        self.game.menus["connect"].deactivate()

    def enterLobby(self):
        self.game.currentMenu = self.game.menus["lobby"]
        self.game.currentMenu.activate()

    def exitLobby(self):
        self.game.menus["lobby"].deactivate()


class Game(object, DirectObject):
    """
    """

    def __init__(self):
        """
        """
        base.disableMouse()
        base.setBackgroundColor(0, 0, 0)
        self.nick = ""
        self.address = ""
        self.currentGame = None
        self.currentMenu = None
        self.gameActive = False
        self.gameState = GameState(self)
        self.client = client.Client(protocols.ClientProtocol())
        self.battle = None
        self.players = self.client.players
        self.menus = {}
        self.commands = {"newGame": (self.gameState.request, ["Match"]),
                         "joinGame": (self.gameState.request, ["JoinMenu"]),
                         "mainMenu": (self.gameState.request, ["MainMenu"]),
                         "connect": (self.connectToServer, []),
                         "exitLobby": (self.gameState.request, ["JoinMenu"]),
                         "setNick": (self.setNick, []),
                         "setAddress": (self.setAddress, []),
                         "enterMessage": (self.enterMessage, []),
                         "nothing": (self.nothing, []),
                         "entNothing": (self.entNothing, [])}
        self.initMenus()

        self.gameState.request("MainMenu")

        self.accept("player_connected", self.playerConnected)
        self.accept("player_disconnected", self.playerDisconnected)
        self.accept("join_accepted", self.joinAccepted)
        self.accept("chat_msg_received", self.chatMsgReceived)
        self.accept("game_started", self.gameStarted)

    def initMenus(self):
        self.menus = menu.initMenus(self.commands, "game")

    def gameStarted(self):
        thisPlayer = self.players[self.nick]
        self.battle = battle.Battle(self.players)
        self.battle.setupWorld(thisPlayer)
        self.gameState.request("Match")

    def connectToServer(self):
        if self.gameState.state == "JoinMenu":
            self.nick = self.currentMenu.controls['ent_nick'].get()
            self.address = self.currentMenu.controls['ent_address'].get()
            if self.nick and self.address:
                connectingSuccessful = self.client.connect(self.address, 12345, 3000, self.nick)
                for o in self.currentMenu.onscreenTexts:
                    o.destroy()

                self.currentMenu.onscreenTexts = []
                self.gameState.request("Lobby")

            else:
                ost = OnscreenText(text="Make sure that server is online and address is correct",
                                   pos=(0, -0.6),
                                   scale=0.07,
                                   fg=(1, 0, 0, 1))
                self.currentMenu.onscreenTexts.append(ost)

    def setNick(self, nick):
        self.nick = nick
        self.currentMenu.controls['ent_address']['focus'] = 1

    def setAddress(self, address):
        self.address = address
        self.connectToServer()

    def playerConnected(self, nick):
        logging.info(nick + " connected")
        if self.gameState.state == "Lobby":
            self.refreshLobby()

    def playerDisconnected(self, nick):
        logging.info(nick + " disconnected")

        if self.gameState.state == "Lobby":
            self.refreshLobby()

    def joinAccepted(self):
        self.refreshLobby()

    def refreshLobby(self):
        for ost in self.currentMenu.onscreenTexts:
            ost.destroy()

        self.currentMenu.onscreenTexts = []

        for i in xrange(len(self.client.nicks)):
            nick = self.client.nicks[i]
            y = 0.65 - i * 0.1
            ost = OnscreenText(text=nick,
                               pos=(-1.2, y),
                               scale=0.07,
                               align=TextNode.ALeft,
                               fg=(0, 1, 0, 1))
            self.currentMenu.onscreenTexts.append(ost)

        showMessagesLen = helper.clamp(len(self.client.chatLog), 0, 10)
        for i in xrange(showMessagesLen):
            nick = self.client.chatLog[i][0]
            msg = self.client.chatLog[i][1]
            y = 0 + (i - showMessagesLen + 1) * 0.07
            text = "<" + nick + "> " + msg
            ost = OnscreenText(text=text,
                               pos=(-1.2, y),
                               scale=0.05,
                               align=TextNode.ALeft,
                               fg=(1, 1, 1, 1))
            self.currentMenu.onscreenTexts.append(ost)

    def enterMessage(self, msg):
        messenger.send("send_chat_message", sentArgs=[msg])
        self.currentMenu.controls['ent_chat'].enterText("")
        self.currentMenu.controls['ent_chat']['focus'] = 1

    def chatMsgReceived(self):
        if self.gameState.state == "Lobby":
            self.refreshLobby()

    def nothing(self):
        pass

    def entNothing(self, text):
        pass

if __name__ == '__main__':
    loadPrcFileData('', 'window-title Client')
    loadPrcFileData('', 'win-origin 0 0')
    import direct.directbase.DirectStart
    logging.basicConfig(filename=os.path.join("..", "client.log"),
                        level=logging.DEBUG)
    logging.info("--------------------- PROGRAM STARTED ------------------")
    global_vars.DUMB = True
    game = Game()
    run()
