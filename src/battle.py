import world
import global_vars


class Battle(object):
    """ Battle handles starting of new rounds, switching to new maps etc.
    """

    def __init__(self, players):
        """
        """
        self.world = None
        self.players = players
        self.battleState = None  # TODO: battleState-tracker here

    def setupWorld(self, thisPlayer):
        self.world = world.World(self.players, thisPlayer)

    def setupServerWorld(self):
        self.world = world.ServerWorld()
        self.setupCasters()

    def setupCasters(self):
        self.world.setupCasters(self.players)
