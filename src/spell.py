#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.
import copy
import logging

from panda3d.core import CollisionSphere, CollisionHandlerEvent, CollisionNode
from direct.task import Task

import gameobj
import manager
import helper
import global_vars


class Spell(object):
    def __init__(self, name, cooldowns, castingRanges, buffs):
        self.name = name
        self.level = 1
        self.cooldowns = cooldowns
        self.cdLeft = cooldowns[0]
        self.castingRanges = castingRanges
        self.buffs = buffs
        self.owner = None
        taskMgr.doMethodLater(0.1, self.countCooldownTask, "CountCooldownTask")

    def __del__(self):
        logging.info("Garbage collected spell!")

    def cast(self):
        pass

    def countCooldownTask(self, task):
        """

        Arguments:
        - `task`:
        """
        if self.cdLeft > 0:
            self.cdLeft -= task.delayTime

        return task.again

    def select(self, owner):
        self.owner = owner
        return self


    def destroy(self):
        self.owner = None
        taskMgr.remove("CountCooldownTask")


class ProjectileSpell(Spell):
    """
    """

    def __init__(self, name, cooldowns, castingRanges, buffs, projectileSpeeds):
        """
        """
        Spell.__init__(self, name, cooldowns, castingRanges, buffs)
        self.projectileSpeeds = projectileSpeeds

    def cast(self, startPosition, direction):
        """
        """
        if self.cdLeft <= 0:
            self.cdLeft = self.cooldowns[0]
            velocity = direction * self.projectileSpeeds[0]
            #projectile = Projectile(startPosition, velocity,
                                    #self.castingRanges[0],
                                    #copy.deepcopy(self.buffs),
                                    #self.owner)
            projectile = manager.DataManager.loadObject('firecube',
                                                        startPosition,
                                                        self.owner.owner)

            projectile.shoot(velocity, self.castingRanges[0],
                             copy.deepcopy(self.buffs))
            # TODO: Make this more generic
            messenger.send("add_game_object", sentArgs=[projectile])
            messenger.send("send_create_object", sentArgs=[
                           "firecube", startPosition, self.owner.owner])

    @staticmethod
    def Fireball():
        burstDamageModifier = StatModifier("hpCurrent", 0, -10, True)
        burstDamageBuff = Buff("burstdamage", [burstDamageModifier])
        return ProjectileSpell("firecube", [0.80], [35.0], [burstDamageBuff], [50.0])


class MovementSpell(Spell):
    def __init__(self, name, cooldowns, castingRanges, buffs, start, end):
        Spell.__init__(self, name, cooldowns, castingRanges, buffs)
        self.startPosition = start
        self.endPosition = end

    @staticmethod
    def Jump():
        pass


class Projectile(gameobj.GameObject):
    def __init__(self, position, owner):
        """
        """
        gameobj.GameObject.__init__(self, position, 'projectile', owner)
        self.pitchCurrent = 0
        self.collisionHandler = CollisionHandlerEvent()
        self.collisionHandler.addInPattern('%fn-in-%in')

        self.accept("projectile-in-character", self.hitCharacter)

    def load(self, dic):
        """
        """
        self.hpMax = dic[u'health_max']
        self.hpCurrent = self.hpMax
        self.speed = dic[u'speed']
        self.turning = dic[u'turning']
        self.model = helper.makeModel(dic[u'model'])
        self.model.reparentTo(render)
        self.model.setPos(self.posCurrent)
        self.collider = self.model.attachNewNode(CollisionNode('projectile'))
        self.collider.setPythonTag("owner", self.owner)
        modelBounds = self.model.getChild(0).getBounds()
        center = modelBounds.getCenter()
        radius = modelBounds.getRadius() * 1.1

        self.collider.node().addSolid(CollisionSphere(center, radius))
        base.cTrav.addCollider(self.collider, self.collisionHandler)

    def shoot(self, velocity, distance, buffs):
        self.distanceCurrent = distance
        self.velocity = velocity
        self.model.reparentTo(render)
        self.buffs = buffs
        taskMgr.add(self.updateTask, "update_task")

    def updateTask(self, task):
        dt = globalClock.getDt()
        dv = self.velocity * dt
        dp = 360 * dt
        self.posCurrent += dv
        self.pitchCurrent += dp
        self.distanceCurrent -= dv.length()
        self.model.setPos(self.posCurrent)
        self.model.setP(self.pitchCurrent)
        # TODO: Add collision check
        if self.distanceCurrent < 0:
            # TODO: Projectile collision GFX here
            if not global_vars.DUMB:
                messenger.send("remove_object", sentArgs=[self])
            self.destroy()
            return task.done

        return task.cont

    def sync(self, data):
        hpCurrent = data[1]
        position = data[2]

        self.hpCurrent = hpCurrent
        self.posCurrent = position
        self.model.setPos(self.posCurrent)

    def hitCharacter(self, entry):
        if global_vars.DUMB:
            return
        intoObj = entry.getIntoNodePath().getPythonTag("owner")
        if entry.getFromNodePath() == self.collider and self.owner.caster != intoObj:
            objType = entry.getIntoNodePath().getTag("object_type")
            if objType in ["character", 'caster']:
                # TODO: Add working buff-system
                intoObj.addBuffs(self.buffs)

            logging.info("Projectile hit object with id %u\n"
                         "which is owned by %s" % (intoObj.id, intoObj.owner.nick))

    def destroy(self):
        """
        """
        self.model.removeNode()
        self.ignoreAll()


class Buff(object):
    def __init__(self, name, statModifiers):
        """
        """
        self.name = name
        self.statModifiers = statModifiers
        self.host = None

    def inflict(self, dt):
        """
        """
        statMods = self.statModifiers[:]
        statModsLen = len(statMods)
        for i in xrange(statModsLen):
            statMod = statMods[i]
            if statMod.duration == 0:
                if statMod.name == "hpCurrent":
                    self.host.hpCurrent += statMod.amount
                else:
                    self.host.__dict__[statMod.name] += statMod.amount
                self.statModifiers.pop(i)
            elif statMod.duration > 0:
                statMod.duration -= dt
                self.host.__dict__[statMod.name] += statMod.amount

                if statMod.duration <= 0:
                    self.host.__dict__[statMod.name] -= statMod.amount
                    self.statModifiers.pop(i)

    def activate(self, host):
        """
        """
        self.host = host


class StatModifier(object):
    def __init__(self, name, duration, amount, permanent=False, overtime=False):
        """
        """
        self._name = name
        self._amount = amount
        self.duration = duration
        self.permanent = permanent
        self.overtime = overtime

    name = property(lambda self: self._name)
    amount = property(lambda self: self._amount)
