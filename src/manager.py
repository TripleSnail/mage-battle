#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

#-*- coding: utf-8 -*-

import logging
import random
import os
import weakref
import json
import posixpath

from panda3d.core import Point3, Vec3
from direct.showbase.DirectObject import DirectObject

import character
import spell
import helper


class GameObjectManager(object, DirectObject):
    """
    """

    def __init__(self, world):
        """
        """
        self.world = world
        self.characters = []
        self.players = {}
        self.gameObjects = weakref.WeakValueDictionary()

    def addGameObject(self, obj):
        logging.info("Adding object with id: " + str(obj.id))
        self.gameObjects[obj.id] = obj

    def removeGameObject(self, obj):
        # TODO: Replace this ugly hack with something better
        if isinstance(obj, (int, long)):
            obj = self.getGameObj(obj)
        obj.destroy()
        if obj.objType in ["character", "caster"]:
            self.characters.remove(obj)

    def getGameObj(self, id):
        obj = None
        try:
            obj = self.gameObjects[id]
        except KeyError:
            logging.error("Tried to get non-existent GameObject with id " + str(id))
        return obj

    # TODO: Consider if nick should be made obligatory
    def createObject(self, name, pos, nick):
        """

        Arguments:
        - `name`: name of object to create
        - `pos`: initial position for object
        - `nick`: nick of player that will own new object
        """
        owner = None

        if nick:
            owner = self.players[nick]
        obj = DataManager.loadObject(name, pos, owner)
        self.addGameObject(obj)

        logging.info("Created object of type %s and with id %u" % (obj.objType, obj.id))

        if obj.objType in ['character', 'caster']:
            self.characters.append(obj)

        if owner and obj.objType == "caster":
            owner.caster = obj
            if (hasattr(self, 'thisPlayer') and
                    self.thisPlayer and owner is self.thisPlayer):
                obj.setPlayerEvents()
                self.world.caster = obj
                logging.info("Object with id " + str(obj.id) + " is players caster")


class ClientGameObjectManager(GameObjectManager):
    def __init__(self, world, thisPlayer):
        GameObjectManager.__init__(self, world)
        self.thisPlayer = thisPlayer
        self.accept("create_object", self.createObject)
        self.accept("sync_id", self.syncId)
        self.accept("remove_object", self.removeGameObject)

    def syncId(self, data):
        id = data[0]
        obj = self.getGameObj(id)
        if obj:
            obj.sync(data)


class ServerGameObjectManager(GameObjectManager):
    def __init__(self, world):
        GameObjectManager.__init__(self, world)
        self.accept("remove_object", self.removeGameObject)

    def removeGameObject(self, obj):
        messenger.send("send_remove_object", sentArgs=[obj])
        GameObjectManager.removeGameObject(self, obj)


class DataManager(object, DirectObject):

    @staticmethod
    def loadObject(name, position=None, owner=None):
        """
        Constructs object which has given name and optionally owner
        Arguments:
        - `name`: name of object to load
        - `owner`: player that owns object
        Returns: loaded object
        """

        if not position:
            position = Point3(random.randint(-45, 45), random.randint(-45, 45), 0)
        name = unicode(name)
        p = posixpath.join(helper.getDataPath(), "common", "objects.json")
        f = open(p, 'r')
        dic = json.load(f)
        f.close()

        dic = dic[name]
        objType = dic[u'object_type']
        obj = None

        # TODO: Consider making this more dynamic
        if objType == u"caster":
            if owner is None:
                obj = character.Warlock(position=position)
            else:
                obj = character.Warlock(position, owner)
            obj.load(dic)

        if objType == u"projectile":
            obj = spell.Projectile(position, owner)
            obj.load(dic)

        if obj is None:
            # TODO: Specify exception
            raise

        return obj
