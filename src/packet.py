#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

#-*- coding: utf-8 -*-
import logging

from direct.showbase.DirectObject import DirectObject
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator
import protocols


class PacketHelper(object, DirectObject):
    """ Base class for CPacketHelper and SPacketHelper.
    """
    def __init__(self):
        pass

    def destroy(self):
        self.ignoreAll()


class ClientPacketHelper(PacketHelper):

    def __init__(self, client):
        """
        """
        self.client = client
        self.accept("clicked_on_floor", self.sendClick)
        self.accept("tried_to_cast", self.sendCastTry)
        self.accept("send_chat_message", self.sendChatMsg)

    def sendClick(self, button, location):
        datagram = PyDatagram()
        datagram.addUint8(protocols.ServerMsgId.mouseClick)
        datagram.addString(button)
        location.writeDatagramFixed(datagram)
        self.client.send(datagram)

    def sendCastTry(self, spellName):
        datagram = PyDatagram()
        datagram.addUint8(protocols.ServerMsgId.spellCastTry)
        datagram.addString(spellName)
        self.client.send(datagram)

    def sendChatMsg(self, message, target="all"):
        datagram = PyDatagram()
        datagram.addUint8(protocols.ServerMsgId.chatMsg)
        datagram.addString(message)
        self.client.send(datagram)


class ServerPacketHelper(PacketHelper):
    def __init__(self, server):
        self.server = server
        self.accept("player_connected", self.sendConnectionNotification)
        self.accept("player_disconnected", self.sendDisconnectionNotification)
        self.accept("send_chat_message", self.sendChatMessage)
        self.accept("send_create_object", self.sendCreateObject)
        self.accept("send_start_game", self.sendStartGame)
        self.accept("send_remove_object", self.sendRemoveObject)
        self.accept("sync_objects", self.syncObjects)

    def sendConnectionNotification(self, nick):
        datagram = PyDatagram()
        datagram.addUint8(protocols.ClientMsgId.playerConnected)
        datagram.addString(nick)
        self.server.broadcast(datagram)

    def sendDisconnectionNotification(self, nick):
        datagram = PyDatagram()
        datagram.addUint8(protocols.ClientMsgId.playerDisconnected)
        datagram.addString(nick)
        self.server.broadcast(datagram)

    def sendChatMessage(self, message, target="all"):
        datagram = PyDatagram()
        datagram.addUint8(protocols.ClientMsgId.chatMsg)
        datagram.addString("server")
        datagram.addString(message)
        if target == "all":
            self.server.broadcast(datagram)

    def syncObjects(self, gameObjects):
        datagram = PyDatagram()
        datagram.addUint8(protocols.ClientMsgId.sync)
        self._addObjectsSyncData(datagram, gameObjects)
        self.server.broadcast(datagram)

    def sendStartGame(self, players):
        datagram = PyDatagram()
        datagram.addUint8(protocols.ClientMsgId.gameStarted)
        nicks = [p.nick for p in players]

        # TODO: Consider replacing buildListStr with another method
        datagram.addString(self.server.protocol.buildListStr(nicks))
        self.server.broadcast(datagram)

    def sendCreateObject(self, name, position, owner):
        datagram = PyDatagram()
        datagram.addUint8(protocols.ClientMsgId.createObject)
        datagram.addString(name)
        position.writeDatagramFixed(datagram)
        if owner:
            datagram.addBool(True)
            datagram.addString(owner.nick)
        else:
            datagram.addBool(False)
        self.server.broadcast(datagram)

    def sendRemoveObject(self, obj):
        logging.info('Sending object destroyal order for object with type %s'
                     'and id %u' % (obj.objType, obj.id))
        datagram = PyDatagram()
        datagram.addUint8(protocols.ClientMsgId.removeObject)
        datagram.addUint16(obj.id)
        self.server.broadcast(datagram)

    def _addObjectsSyncData(self, datagram, gameObjects):
        datagram.addUint16(len(gameObjects))
        for id, obj in gameObjects.iteritems():
            datagram.addUint16(obj.id)
            datagram.addUint16(obj.hpCurrent)
            obj.posCurrent.writeDatagramFixed(datagram)
