#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import sys
from panda3d.core import *

sys.path.append("../src/")
import helper


class HelperFunctionTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_angleToVec2(self):

        vec1 = Vec2(1, 0)
        vec2 = Vec2(1, -1); vec2.normalize()
        
        self.assertEqual(vec1, helper.angleToVec2(0))
        self.assertEqual(vec2, helper.angleToVec2(45))

    def test_vec2ToAngle(self):
        pass

    def test_clamp(self):
        min = -4.4
        max = 7.122
        
        lesser = -5.8
        between = -1.0
        greater = 10.0

        self.assertEqual(min, helper.clamp(lesser, min, max))
        self.assertEqual(between, helper.clamp(between, min, max))
        self.assertEqual(max, helper.clamp(greater, min, max))
        

    def test_makeActor(self):
        pass

    def test_makeModel(self):
        pass


if __name__ == '__main__':
    # unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(HelperFunctionTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
