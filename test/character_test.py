#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import sys
from panda3d.core import *
loadPrcFileData("", "window-type none")

sys.path.append("../src/")
import character

class CharacterTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_init_Character_normal(self):
        pos = Point3(5.0, 0, 2.8)
        char = character.Character(pos)

        self.assertTrue(char.posCurrent.almostEqual(pos))

class WarlockTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_init_Warlock_normal(self):
        pos = Point3(5.0, 0, 2.8)
        war = character.Warlock(position=pos)
        self.assertTrue(war.posCurrent.almostEqual(pos))

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(CharacterTests)
    unittest.TextTestRunner(verbosity=2).run(suite)

    suite2 = unittest.TestLoader().loadTestsFromTestCase(WarlockTests)
    unittest.TextTestRunner(verbosity=2).run(suite2)

