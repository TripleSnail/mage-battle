#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import sys
from panda3d.core import *

from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator

sys.path.append("../src/")

import packet

class PacketHelperTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_addXYZ_with_integers(self):
        datagram = PyDatagram()
        x = 4
        y = 5
        z = 12
        testVec = Vec3(x, y, z)

        packet.PacketHelper._addXYZ(datagram, testVec)
        it = PyDatagramIterator(datagram)
        self.assertEqual(x, it.getFloat64())
        self.assertEqual(y, it.getFloat64())
        self.assertEqual(z, it.getFloat64())

    def test_addXYZ_with_floats(self):
        datagram = PyDatagram()
        x = 2.5
        y = 7.3
        z = 2.1
        testVec = Vec3(x, y, z)

        packet.PacketHelper._addXYZ(datagram, testVec)
        it = PyDatagramIterator(datagram)

        # With precision of 7 decimal places, tests always fail with
        # these numbers
        self.assertAlmostEqual(x, it.getFloat64(), places=5)
        self.assertAlmostEqual(y, it.getFloat64(), places=5)
        self.assertAlmostEqual(z, it.getFloat64(), places=5)

    def test_addXYZ_with_mixed(self):
        datagram = PyDatagram()
        x = 2.5
        y = 7
        z = 2
        testVec = Vec3(x, y, z)

        packet.PacketHelper._addXYZ(datagram, testVec)
        it = PyDatagramIterator(datagram)

        # With precision of 7 decimal places, tests always fail with
        # these numbers
        self.assertAlmostEqual(x, it.getFloat64(), places=5)
        self.assertAlmostEqual(y, it.getFloat64(), places=5)
        self.assertAlmostEqual(z, it.getFloat64(), places=5)

    def test_addUint8List_with_normal_uints(self):
        datagram = PyDatagram()
        someUints = [4, 7, 8, 12]

        packet.PacketHelper.addUint8List(datagram, someUints)
        it = PyDatagramIterator(datagram)
        self.assertEqual(len(someUints), it.getUint16())
        for uint in someUints:
            self.assertEqual(uint, it.getUint8())

    def test_addUint8List_with_empty_uint_list(self):
        datagram = PyDatagram()
        emptyUintList = []

        packet.PacketHelper.addUint8List(datagram, emptyUintList)
        it = PyDatagramIterator(datagram)
        self.assertEqual(0, it.getUint16())

        # Make sure that asking for more causes error
        with self.assertRaises(AssertionError):
            it.getUint8()

    def test_getUint8List_normal(self):
        datagram = PyDatagram()
        someUints = [1, 35, 23, 121, 89]
        datagram.addUint16(len(someUints))
        for uint in someUints:
            datagram.addUint8(uint)

        it = PyDatagramIterator(datagram)
        retList = packet.PacketHelper.getUint8List(it)

        self.assertEqual(someUints, retList)

    def test_getXYZ_normal(self):
        x = 5
        y = 7
        z = 12

        datagram = PyDatagram()
        datagram.addFloat64(x)
        datagram.addFloat64(y)
        datagram.addFloat64(z)
        it = PyDatagramIterator(datagram)
        self.assertEqual((x, y, z), packet.PacketHelper.getXYZ(it))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(PacketHelperTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
