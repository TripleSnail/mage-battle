#This file is part of Mage Battle.

#Mage Battle is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Mage Battle is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Mage Battle.  If not, see <http://www.gnu.org/licenses/>.

import unittest
import sys
import gc
from panda3d.core import Point3, loadPrcFileData
loadPrcFileData("", "window-type none")

import direct.directbase.DirectStart

sys.path.append("../src/")
import manager
import gameobj
import character
import helper

helper.setDataPath("../data")

# TODO: Try to run tests inside Panda3d's main loop
class GameObjectManagerTests(unittest.TestCase):
    def setUp(self):
        self.man = manager.GameObjectManager(None, None)

    def tearDown(self):
        for v in self.man.gameObjects.values():
            self.man.removeGameObject(v)
        gc.collect()
        reload(gameobj)
        reload(character)
        self.man.ignoreAll()

    def test_createObject_single_normal_empty_nick(self):
        objName = "warlock"
        position = Point3(5.0, 20.0, 0)
        nick = ""

        self.man.createObject(objName, position, nick)
        obj = self.man.getGameObj(0)

        self.assertIsNotNone(obj)
        self.assertTrue(position.almostEqual(obj.posCurrent))
        # NOTE: Following "magic numbers" are from data/common/objects.json
        self.assertEqual(obj.speedMovement, 20)
        self.assertEqual(obj.speedRotation, 360)


    def test_createObject_single_normal_empty_nick_event(self):
        objName = "warlock"
        position = Point3(5.0, 20.0, 0)
        nick = ""

        messenger.send("create_object", [objName, position, nick])

        obj = self.man.getGameObj(0)

        self.assertIsNotNone(obj)
        self.assertTrue(position.almostEqual(obj.posCurrent))
        # NOTE: Following "magic numbers" are from data/common/objects.json
        self.assertEqual(obj.speedMovement, 20)
        self.assertEqual(obj.speedRotation, 360)

    def test_removeGameObject(self):
        objName = "warlock"
        position = Point3(5.0, 20.0, 0)
        nick = ""

        messenger.send("create_object", [objName, position, nick])

        obj = self.man.getGameObj(0)
        id = obj.id

        self.man.removeGameObject(obj)
        del obj
        gc.collect()

        #self.assertIsNone(self.man.getGameObj(id))
        #print sys.getrefcount(self.man.getGameObj(id))
        with self.assertRaises(KeyError):
            self.man.getGameObj(id)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(GameObjectManagerTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
